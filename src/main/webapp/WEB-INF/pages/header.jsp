<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="navbar">
    <div class="navbar-inner">
        <div class="container">
            <ul class="nav nav-pills">
                <li><a href="${pageContext.request.contextPath}/index.html"><spring:message code="label.home"/></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <spring:message code="label.organizations"/>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="${pageContext.request.contextPath}/organization/add.html"><spring:message code="label.add"/></a></li>
                        <li><a href="${pageContext.request.contextPath}/organization/list.html"><spring:message code="label.list"/></a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <spring:message code="label.teams"/>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="${pageContext.request.contextPath}/team/add.html"><spring:message
                                code="label.add"/></a></li>
                        <li><a href="${pageContext.request.contextPath}/team/list.html"><spring:message
                                code="label.list"/></a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <spring:message code="label.teamMembers"/>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="${pageContext.request.contextPath}/member/add.html"><spring:message code="label.add"/></a></li>
                        <li><a href="${pageContext.request.contextPath}/member/list.html"><spring:message code="label.list"/></a></li>
                    </ul>
                </li>
            </ul>
            <p class="navbar-text navbar-right">Signed in as <a href="https://www.facebook.com/juripronin"

                                                                class="navbar-link">Yury Pronin </a>
                <span style="float: right">
    <a href="?lang=en">en</a>
    |
    <a href="?lang=ru">ru</a>
</span></p>
        </div>
    </div>
</div>
