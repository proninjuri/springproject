package com.sprhib.dao;

import com.sprhib.model.Organization;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrganizationDAO {


    @Autowired
    private SessionFactory sessionFactory;

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    public void addOrganization(Organization organization) {
        getCurrentSession().saveOrUpdate(organization);
    }


    @SuppressWarnings("unchecked")
    public List<Organization> getOrganizations() {
        return getCurrentSession().createQuery("from Organization").list();
    }

    public Organization getOrganization(Integer id) {
        Organization organization = (Organization) getCurrentSession().get(Organization.class, id);
        return organization;
    }

    public void updateOrganization(Organization organization) {
        Organization organizationToUpdate = getOrganization(organization.getId());
        organizationToUpdate.setName(organization.getName());
        organizationToUpdate.setTeams(organization.getTeams());
        getCurrentSession().update(organizationToUpdate);
    }

    public void deleteOrganization(Integer id) {
        Organization organization = getOrganization(id);
        if (organization != null) {
            getCurrentSession().delete(organization);
        }
    }

}
