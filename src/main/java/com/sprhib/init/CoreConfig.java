package com.sprhib.init;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
@Import(value = {DatabaseConfig.class, FlywayConfig.class, HibernateConfig.class})
@ComponentScan(value = {"com.sprhib.dao", "com.sprhib.service"})
public class CoreConfig {

}
