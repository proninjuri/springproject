package com.sprhib.service;

import com.sprhib.model.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

/**
 * Created by Juri on 17/09/14.
 */
public class TeamFormatter implements Formatter<Team> {

    @Autowired
    TeamService teamService;

    @Override
    public Team parse(String id, Locale locale) throws ParseException {
        return teamService.getTeam(Integer.parseInt(id));
    }

    @Override
    public String print(Team team, Locale locale) {
        return team.getName();
    }
}
